Source: pwru
Section: golang
Priority: optional
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders:
 Loren M. Lang <lorenl@north-winds.org>,
Rules-Requires-Root: no
Build-Depends:
 debhelper-compat (= 13),
 clang,
 dh-sequence-golang,
 golang-any,
 golang-github-cheggaaa-pb.v3-dev (>= 3.1.4),
 golang-github-cilium-ebpf-dev (>= 0.11.0),
 golang-github-cloudflare-cbpfc-dev (>= 0.0~git20221018),
 golang-github-jsimonetti-rtnetlink-dev (>= 1.4.0),
 golang-github-mitchellh-go-ps-dev (>= 0.0~git20150710.0.e6c6068),
 golang-github-spf13-pflag-dev (>= 1.0.5),
 golang-github-vishvananda-netns-dev (>= 0.0.4),
 golang-golang-x-net-dev (>= 1:0.19.0),
 golang-golang-x-sys-dev (>= 0.15.0),
 libpcap0.8-dev,
Testsuite: autopkgtest-pkg-go
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/go-team/packages/pwru
Vcs-Git: https://salsa.debian.org/go-team/packages/pwru.git
Homepage: https://github.com/cilium/pwru
XS-Go-Import-Path: github.com/cilium/pwru

Package: pwru
Section: net
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Built-Using: ${misc:Built-Using},
Description: eBPF-based Linux kernel networking debugger (program)
 pwru is an eBPF (https://ebpf.io)-based tool for tracing network packets
 in the Linux kernel with advanced filtering capabilities. It allows fine-
 grained introspection of kernel state to facilitate debugging network
 connectivity issues.
 .
 pwru requires >= 5.3 kernel to run. debugfs has to be mounted in
 /sys/kernel/debug.

